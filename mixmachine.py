# -*- mode: python; coding: utf-8 ; python-indent: 2 -*-
#!/usr/bin/python3

# Vincent Rioux, mars 2018

import sys, os, glob, shutil, mimetypes, random, traceback
import time, math
from threading import Thread

import mpv
"""
pip3 install python-mpv
sudo apt install mpv libmpv1
TODO : 
- check for not repeating choice


m = mpv.MPV(ao="jack", jack_name="bla")
m.play("/data/audio/max2.wav") 
m.volume = 30
m.duration
m.terminate()
"""

global tree

def _getDirs(base):
  return [x for x in glob.iglob(os.path.join( base, '*')) if os.path.isdir(x) ]

def rglob(base, pattern):
  list = []
  list.extend(glob.glob(os.path.join(base,pattern)))
  dirs = _getDirs(base)
  if len(dirs):
    for d in dirs:
      list.extend(rglob(os.path.join(base,d), pattern))
  return list

class Player():
  def __init__(self, index):
    global tree
    self.index = index
    self.player_name = "mixmachine"+str(index)
    # self.player = mpv.MPV(ao = "jack", jack_name = self.player_name)
    self.player = mpv.MPV(ao = "jack")
    self.t = Thread(target=self.loop)
    self.t.start()

  def loop(self):
    self.running = True
    while self.running:
      mt = ""
      totalDur = None
      while (("audio" not in mt) or (totalDur == None)):
        f = random.choice(tree)
        mt = str(mimetypes.guess_type(f)[0])
        if ("audio" in mt):
          self.player.volume = 0
          self.player.play(f)
          time.sleep(0.5) # need some time to read file and parse total duration (but if too long file is played already...
          totalDur = self.player.duration

      print(f, "total dur :", totalDur)
      pos = random.random() * totalDur
      dur = random.random()*(totalDur-pos) + 0.1
      vol = (random.random()*0.6+0.4) * 100

      print(f, "pos : ",pos, "total dur :", totalDur, "dur : ", dur, "vol : ", vol)
      self.player.seek(pos,reference="absolute")
      self.player.volume = vol
      dur0 = 0
      while ((self.running) and (dur0<dur)):
        time.sleep(1)
        dur0+=1

  def quit(self):
    self.running = False
    self.player.terminate()

    
if len(sys.argv) == 2:
  time.sleep(1)
  print ("Use ctrl-C to quit.")
  rep = os.path.abspath(sys.argv[1])
  print(rep)
  global tree
  tree = rglob(rep, '*')

  max_layers = 10
  players = []
  # build the matrix of players
  for i in range(max_layers):
    p = Player(i)
    players.append(p)
  
  input("press enter to quit")
  for mp in players:
    mp.quit()
    
else:
  print("usage: python mixmachine.py directory")
